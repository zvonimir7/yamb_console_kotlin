var gameOver: Boolean = false
const val numberOfDices: Int = 6

fun main() {
    while (!gameOver) {
        println("Game is starting!")
        Thread.sleep(3000)
        println("I will roll Your dices now.\n")
        Thread.sleep(2000)
        println("These are Your dices after first roll:")
        val diceWorker = DiceWorker(numberOfDices)
        val gameController = GameController(diceWorker)
        diceWorker.rollAllDices()
        println("-----------------------------")
        diceWorker.showDicesStats()

        for (i in 2..3) {
            println("To change state which dice to put aside or pull them back to active ones, write their ID/IDs.")
            println("Example: write \"1,4\" to put first and fourth dice aside or to pull them back if they are already aside.")
            println("Write \"skip\" if You don't want to throw any dices or change their states anymore.")
            println("Write \"roll\" or leave input empty to instantly roll all active dices")
            print("Your choice to roll, skip or input which dices to change their state: ")
            gameController.currentRound++
            val userInput = readLine()

            if (userInput == "roll") {
                diceWorker.rollAllDices()
                println()
                diceWorker.showDicesStats()
                continue
            } else if (userInput == "skip") {
                break
            } else {
                val numbersOnly = userInput?.let { //extracting only numbers [0-6] from user input (for dice ID)
                    Regex("[1-6]").findAll(it)
                        .map(MatchResult::value)
                        .toList()
                }
                val result = numbersOnly?.map { it.toInt() }
                if (result != null) {
                    diceWorker.changeStateOfDices(result)
                } else continue
                diceWorker.rollAllDices()
                println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
                println("\nThese are Your dices after $i. roll:")
                println("-----------------------------")
                diceWorker.showDicesStats()
            }
        }

        gameController.checkForBonuses()
        gameController.currentRound = 0
        println("Total score after bonuses is: ${gameController.totalScore}")

        println("\nDo You wanna play another round? Yes/No(!yes)")
        print("Your answer: ")
        if (readLine()?.toLowerCase() != "yes") {
            gameOver = true
            println("GAME HAS ENDED!")
        } else
            println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
    }
}