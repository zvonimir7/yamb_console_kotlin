class DiceWorker(numberOfDices: Int) {
    private var Dices: MutableMap<Int, Dice> = mutableMapOf()
    private var NumberOfDicesInBox: Int = numberOfDices

    constructor() : this(1)

    init {
        for (i in 1..(this.NumberOfDicesInBox)) {
            this.Dices[i] = Dice(i)
        }
    }

    fun addDiceToBox(dice: Dice) {
        this.Dices[this.NumberOfDicesInBox] = dice
        this.NumberOfDicesInBox++
    }

    fun showDicesStats() {
        for (dice in this.Dices) {
            println(dice.value)
        }
    }

    fun rollAllDices() {
        for (dice in this.Dices) {
            dice.value.rollDice()
        }
    }

    fun getDicesBox(): MutableMap<Int, Dice> {
        return this.Dices
    }

    fun getListOfDiceValues(): MutableList<Int> {
        val diceRolledNumbers: MutableList<Int> = mutableListOf()
        for (dice in this.Dices) {
            diceRolledNumbers.add(dice.value.RolledNumber)
        }
        return diceRolledNumbers
    }

    fun changeStateOfDices(lockedDices: List<Int>) {
        for (i in lockedDices) {
            this.Dices[i]?.changeLockedState()
        }
    }
}